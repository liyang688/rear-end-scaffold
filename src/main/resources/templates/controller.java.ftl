package ${package.Controller};


import org.springframework.web.bind.annotation.RequestMapping;

<#if restControllerStyle>
    import org.springframework.web.bind.annotation.RestController;
<#else>
    import org.springframework.stereotype.Controller;
</#if>
<#if superControllerClassPackage??>
    import ${superControllerClassPackage};
</#if>

/**
* <p>
    * ${table.comment!} 前端控制器
    * </p>
* @Design by ly
* @author ${author}
* @since ${date}
*/
<#if restControllerStyle>
    @RestController
<#else>
    @Controller
</#if>
@Api(tags = "xxx")
@RequestMapping("<#if package.ModuleName??>/${package.ModuleName}</#if>/<#if controllerMappingHyphenStyle??>${controllerMappingHyphen}<#else>${table.entityPath}</#if>")
<#if kotlin>
    class ${table.controllerName}<#if superControllerClass??> : ${superControllerClass}()</#if>
<#else>
    <#if superControllerClass??>
        public class ${table.controllerName} extends ${superControllerClass} {
    <#else>
        public class ${table.controllerName} {
    </#if>

    @Autowired
    private ${table.serviceName}  i${entity}Service;

    /**
    * 分页查所有
    *
    * @return
    */
    @GetMapping
    @ApiOperation(value = "分页查询所有", notes = "")
    public Result getAll(@RequestParam(value = "start", defaultValue = "1") int start,
    @RequestParam(value = "size", defaultValue = "10") int size) {
    Page<${entity}> page = new Page<>();
    page.setSize(size);
    page.setCurrent(start);
    IPage<${entity}> resultPage = i${entity}Service.page(page, new QueryWrapper<${entity}>()
    .orderByDesc("id"));
    return Result.success(resultPage);
    }

    /**
    * 不分页查询所有
    * @return
    */
    @GetMapping("/list")
    @ApiOperation(value = "不分页查询所有")
    public Result list() {
    return Result.success(i${entity}Service.list(new QueryWrapper<${entity}>()
    .orderByDesc("id")));
    }

    /**
    * 查一个
    * @param id
    * @return
    */
    @GetMapping("/getOne")
    @ApiOperation(value = "查一个", notes = "")
    public Result getOne(@RequestParam(value = "id", defaultValue = "1") int id){
    ${entity} ${table.entityPath} = i${entity}Service.getById(id);
    return Result.success(${table.entityPath});
    }


    /**
    * 增
    *
    * @return
    */
    @PostMapping
    @ApiOperation(value = "增", notes = "")
    public Result insert(@RequestBody ${entity} ${table.entityPath}) {
    boolean b = i${entity}Service.save(${table.entityPath});
    if (b) {
    return Result.success("添加成功！");
    } else {
    return Result.error(-1, "添加失败！");
    }

    }

    /**
    * 删
    *
    * @param id
    * @return
    */
    @DeleteMapping
    @ApiOperation(value = "删", notes = "传一个id即可")
    public Result delete(@RequestParam(value = "id") int id) {
    boolean b = i${entity}Service.removeById(id);
    if (b) {
    return Result.success("删除成功！");
    } else {
    return Result.error(-1, "删除失败！");
    }
    }

    /**
    * 删一批
    *
    * @param
    * @return
    */
    @DeleteMapping("/deleteBatch")
    @ApiOperation(value = "删一批", notes = "")
    public Result deleteBatch(@RequestBody DeleteDto deleteDto) {
    boolean b = i${entity}Service.removeByIds(deleteDto.getIds());
    if (b) {
    return Result.success("删除成功！");
    } else {
    return Result.error(-1, "删除失败！");
    }
    }

    /**
    * 改
    *
    * @return
    */
    @PutMapping
    @ApiOperation(value = "改", notes = "传一个对象和一个id")
    public Result update(@RequestBody ${entity} ${table.entityPath}) {

    boolean b = i${entity}Service.updateById(${table.entityPath});
    if (b) {
    return Result.success("修改成功！");
    } else {
    return Result.error(-1, "修改失败！");
    }
    }


    }
</#if>
