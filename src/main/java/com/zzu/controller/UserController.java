package com.zzu.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zzu.common.dto.DeleteDto;
import com.zzu.common.dto.LoginDto;
import com.zzu.common.lang.Result;
import com.zzu.entity.User;
import com.zzu.service.IUserService;
import com.zzu.util.TokenUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.mail.HtmlEmail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;

import javax.mail.internet.*;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import javax.mail.*;
import java.util.Properties;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ly
 * @since 2021-11-16
 */
@RestController
@RequestMapping("/user")
@Api(tags = "用户管理模块",value = "")
public class UserController {

    @Autowired
    private IUserService iUserService;

    @PostMapping("/login")
    @ApiOperation(value = "登录", notes = "只需要传username，password")
    public Result login(@RequestBody LoginDto loginDto) {
        String username = loginDto.getUsername();
        String password = loginDto.getPassword();
        //判断是否为密码不对
        User oneUser = iUserService.getOne(new QueryWrapper<User>()
                .eq("username", username)
                .ne("password", DigestUtils.md5DigestAsHex(password.getBytes())));
        if (oneUser != null) {
            return Result.error(-2, "密码不正确！");
        }

        User userTemp = iUserService.getOne(new QueryWrapper<User>()
                .eq("username", username)
                .eq("password", DigestUtils.md5DigestAsHex(password.getBytes())));
        if (userTemp == null) {
            return Result.error("登陆失败！");
        }
        String token = TokenUtil.sign(userTemp);
        //构造结果
        Map<String, Object> map = new HashMap<>();
        map.put("id", userTemp.getUserId());
        map.put("username", userTemp.getUsername());
        map.put("token", token);
        map.put("role", userTemp.getRole());
        return Result.success(map, "登陆成功！");
    }

    /**
     * 新增
     *
     * @param user
     * @return
     */
    @PostMapping
    @ApiOperation(value = "注册用户", notes = "传一个user对象 只要username password 头像url")
    public Result insert(@RequestBody User user) {
        List<User> users = iUserService.list(new QueryWrapper<User>().eq("username", user.getUsername()));
        if (users.size() >= 1) {
            return Result.error("用户名已存在！");
        }
        user.setPassword(DigestUtils.md5DigestAsHex(user.getPassword().getBytes()));
        user.setRole("user");
        LocalDateTime time = LocalDateTime.now();
        user.setRegisterTime(time);
        boolean b = iUserService.save(user);
        if (b) {
            return Result.success("添加成功！");
        } else {
            return Result.error(-1, "添加失败！");
        }

    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @DeleteMapping
    @ApiOperation(value = "删除用户", notes = "传一个id即可")
    public Result delete(@RequestParam(value = "id") int id) {
        boolean b = iUserService.removeById(id);
        if (b) {
            return Result.success("删除成功！");
        } else {
            return Result.error(-1, "删除失败！");
        }
    }

    /**
     * 修改
     *
     * @param user
     * @return
     */
    @PutMapping
    @ApiOperation(value = "修改用户", notes = "传一个对象和一个id")
    public Result update(@RequestBody User user) {

        boolean b = iUserService.updateById(user);
        if (b) {
            return Result.success("修改成功！");
        } else {
            return Result.error(-1, "修改失败！");
        }

    }


    /**
     * 查询所有用户
     *
     * @return
     */
    @GetMapping
    @ApiOperation(value = "分页查询所有用户", notes = "")
    public Result getAll(@RequestParam(value = "start", defaultValue = "1") int start,
                         @RequestParam(value = "nickname", defaultValue = "") String nickname,
                         @RequestParam(value = "size", defaultValue = "10") int size) {
        Page<User> page = new Page<>();
        page.setSize(size);
        page.setCurrent(start);
        IPage<User> resultPage = iUserService.page(page, new QueryWrapper<User>()
                .orderByDesc("user_id")
                .eq(!nickname.equals(""), "nickname", nickname));
        return Result.success(resultPage);
    }


    @PutMapping("/resetPassword")
    @ApiOperation(value = "重置密码", notes = "传入验证码和新的密码")
    public Result resetPassword(@RequestParam(value = "username") String username,
                                @RequestParam(value = "code") String code,
                                @RequestParam(value = "newPassword") String newPassword) {

        boolean b = iUserService.update(new UpdateWrapper<User>()
                .eq("code", code)
                .eq("username", username)
                .set("password", DigestUtils.md5DigestAsHex(newPassword.getBytes())));
        if (b) {
            return Result.success("密码修改成功！");
        } else {
            return Result.error(-1, "密码修改失败！");
        }

    }

    /**
     * 当前用户
     *
     * @param httpServletRequest
     * @return
     */
    @GetMapping("/currentUser")
    @ApiOperation(value = "获取当前用户", notes = "")
    public Result getUnamByToken(HttpServletRequest httpServletRequest) {
        String currentUsername = TokenUtil.getCurrentUsername(httpServletRequest);
        User user = iUserService.getOne(new QueryWrapper<User>().eq("username", currentUsername));
        return Result.success(user);
    }

    /**
     * 查一个
     *
     * @param nickname
     * @return
     */
    @GetMapping("/getOne")
    @ApiOperation(value = "根据nickname查询", notes = "")
    public Result getOne(@RequestParam(value = "nickname", defaultValue = "") String nickname) {
        List<User> users = iUserService.list(new QueryWrapper<User>().eq("nickname", nickname));
        return Result.success(users);
    }

    @ApiOperation(value = "查看单个用户", notes = "")
    @GetMapping("/{id}")
    public Result delete(@PathVariable("id") Integer id) {

        User user = iUserService.getById(id);
        if (user == null) {
            return Result.error("无用此用户！");
        }
        return Result.success(user);

    }

    @ApiOperation(value = "退出", notes = "")
    @GetMapping("logout")
    public Result logout() {

        return Result.success("退出成功！");

    }

    /**
     * 删一批
     *
     * @param
     * @return
     */
    @DeleteMapping("/deleteBatch")
    @ApiOperation(value = "删一批", notes = "")
    public Result deleteBatch(@RequestBody DeleteDto deleteDto) {
        boolean b = iUserService.removeByIds(deleteDto.getIds());
        if (b) {
            return Result.success("删除成功！");
        } else {
            return Result.error(-1, "删除失败！");
        }
    }
}