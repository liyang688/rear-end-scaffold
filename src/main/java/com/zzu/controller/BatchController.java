package com.zzu.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zzu.common.dto.DeleteDto;
import com.zzu.common.lang.Result;
import com.zzu.entity.Batch;
import com.zzu.service.IBatchService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author xxx
 * @Design by ly
 * @since 2022-09-29
 */
@RestController
@Api(tags = "xxx")
@RequestMapping("/batch")
public class BatchController {

    @Autowired
    private IBatchService iBatchService;

    /**
     * 分页查所有
     *
     * @return
     */
    @GetMapping
    @ApiOperation(value = "分页查询所有", notes = "")
    public Result getAll(@RequestParam(value = "start", defaultValue = "1") int start,
                         @RequestParam(value = "size", defaultValue = "10") int size) {
        Page<Batch> page = new Page<>();
        page.setSize(size);
        page.setCurrent(start);
        IPage<Batch> resultPage = iBatchService.page(page, new QueryWrapper<Batch>()
                .orderByDesc("id"));
        return Result.success(resultPage);
    }

    /**
     * 不分页查询所有
     *
     * @return
     */
    @GetMapping("/list")
    @ApiOperation(value = "不分页查询所有")
    public Result list() {
        return Result.success(iBatchService.list(new QueryWrapper<Batch>()
                .orderByDesc("id")));
    }

    /**
     * 查一个
     *
     * @param id
     * @return
     */
    @GetMapping("/getOne")
    @ApiOperation(value = "查一个", notes = "")
    public Result getOne(@RequestParam(value = "id", defaultValue = "1") int id) {
        Batch batch = iBatchService.getById(id);
        return Result.success(batch);
    }


    /**
     * 增
     *
     * @return
     */
    @PostMapping
    @ApiOperation(value = "增", notes = "")
    public Result insert(@RequestBody Batch batch) {
        boolean b = iBatchService.save(batch);
        if (b) {
            return Result.success("添加成功！");
        } else {
            return Result.error(-1, "添加失败！");
        }

    }

    /**
     * 删
     *
     * @param id
     * @return
     */
    @DeleteMapping
    @ApiOperation(value = "删", notes = "传一个id即可")
    public Result delete(@RequestParam(value = "id") int id) {
        boolean b = iBatchService.removeById(id);
        if (b) {
            return Result.success("删除成功！");
        } else {
            return Result.error(-1, "删除失败！");
        }
    }

    /**
     * 删一批
     *
     * @param
     * @return
     */
    @DeleteMapping("/deleteBatch")
    @ApiOperation(value = "删一批", notes = "")
    public Result deleteBatch(@RequestBody DeleteDto deleteDto) {
        boolean b = iBatchService.removeByIds(deleteDto.getIds());
        if (b) {
            return Result.success("删除成功！");
        } else {
            return Result.error(-1, "删除失败！");
        }
    }

    /**
     * 改
     *
     * @return
     */
    @PutMapping
    @ApiOperation(value = "改", notes = "传一个对象和一个id")
    public Result update(@RequestBody Batch batch) {

        boolean b = iBatchService.updateById(batch);
        if (b) {
            return Result.success("修改成功！");
        } else {
            return Result.error(-1, "修改失败！");
        }
    }


}
