package com.zzu.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 资源映射路径
 */
@Configuration
public class MyWebAppConfigurer implements WebMvcConfigurer {
    private String resourceUrl = System.getProperty("user.dir")+System.getProperty("file.separator")
                            +"resource"+System.getProperty("file.separator");

    //头像图片映射
    String imgPath = System.getProperty("user.dir")+System.getProperty("file.separator")
            +"image"+System.getProperty("file.separator"); // 头像上传后的路径
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
         registry.addResourceHandler("/resource/**")
                .addResourceLocations("file:"+resourceUrl);

    }
}
