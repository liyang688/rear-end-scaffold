package com.zzu.service;

import com.zzu.entity.Batch;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xxx
 * @since 2022-09-29
 */
public interface IBatchService extends IService<Batch> {

}
