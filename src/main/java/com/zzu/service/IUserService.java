package com.zzu.service;

import com.zzu.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ly
 * @since 2021-11-25
 */
public interface IUserService extends IService<User> {
     String achieveCode();
}
