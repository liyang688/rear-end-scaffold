package com.zzu.interceptor;


import com.zzu.common.lang.Result;
import com.zzu.util.TokenUtil;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

@Component
public class TokenInterceptor implements HandlerInterceptor {


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)throws Exception{

        if(request.getMethod().equals("OPTIONS")){
            response.setStatus(HttpServletResponse.SC_OK);
            return true;
        }

        response.setCharacterEncoding("utf-8");

        String token = request.getHeader("token");

        //1.token行的时候
        if(token != null){
            boolean result = TokenUtil.verify(token);
            if(result){
                //System.out.println("通过拦截器");
                return true;
            }
        }
        //2. token为null
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        PrintWriter out = null;
        //System.out.println("==== 未通过拦截器=====");
        try{
//            JSONObject json = new JSONObject();
//            json.put("success","false");
//            json.put("msg","认证失败，未通过拦截器");
//            json.put("code","50000");
// response.getWriter().append(json.toString());
            response.getWriter().append(Result.error(401,"token过期！").toString());
            //System.out.println("认证失败，未通过拦截器");
            // response.getWriter().write("50000");
        }catch (Exception e){
            e.printStackTrace();
            //response.sendError(500);
            response.sendError(401,"token过期");
            return false;
        }

        response.sendError(401,"token过期");
        return false;

    }





}
