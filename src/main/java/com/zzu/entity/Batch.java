package com.zzu.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author xxx
 * @since 2022-09-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Batch implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 任务id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * hbase 批次id
     */
    @TableField("batchId")
    private String batchId;

    /**
     * 批次名称
     */
    @TableField("batchName")
    private String batchName;

    /**
     * 批次类型
     */
    @TableField("batchType")
    private String batchType;

    /**
     * 插入中 未开始  进行中  已暂停  已完成  执行失败
     */
    @TableField("batchState")
    private String batchState;

    /**
     * 操作者
     */
    private String operator;

    /**
     * 任务创建时间
     */
    @TableField("createTime")
    private LocalDateTime createTime;

    /**
     * 任务开始执行时间
     */
    @TableField("startTime")
    private LocalDateTime startTime;

    /**
     * 任务执行完毕时间
     */
    @TableField("endTime")
    private LocalDateTime endTime;

    /**
     * 批次细节
     */
    @TableField("batchDetail")
    private String batchDetail;

    /**
     * 算法
     */
    private String algorithm;

    /**
     * 已完成的数目，每次完成后加一
     */
    private Integer finished;

    /**
     * 总计数目
     */
    @TableField("totalNum")
    private Integer totalNum;

    /**
     * 优先级
     */
    private Long priority;

    /**
     * 单弹药类型
     */
    @TableField("ammoType")
    private String ammoType;

    /**
     * 目标类型
     */
    @TableField("targetType")
    private String targetType;


}
