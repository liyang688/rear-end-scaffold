package com.zzu.mapper;

import com.zzu.entity.Batch;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xxx
 * @since 2022-09-29
 */
public interface BatchMapper extends BaseMapper<Batch> {

}
