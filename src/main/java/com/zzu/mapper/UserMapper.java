package com.zzu.mapper;

import com.zzu.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ly
 * @since 2021-11-25
 */
public interface UserMapper extends BaseMapper<User> {

}
