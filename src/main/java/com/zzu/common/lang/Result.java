package com.zzu.common.lang;
import lombok.Data;
import java.io.Serializable;

/**
 * Created on 2020/11/30.
 *
 * @author allen4tech
 */
@Data
public class Result implements Serializable {
    /**
     * 响应代码
     */
    private Integer code;

    /**
     * 响应消息
     */
    private String message;

    /**
     * 响应结果
     */
    private Object data;


    /**
     * 成功
     *
     * @return
     */
    public static Result success() {
        return success(null);
    }

    /**
     * 成功
     * @param data
     * @return
     */
    public static Result success(Object data) {

        Result rb = new Result();
        rb.setCode(ResultInfo.SUCCESS.getCode());
        rb.setMessage(ResultInfo.SUCCESS.getMsg());
        rb.setData(data);
        return rb;

    }


    /**
     * 成功
     * @param data
     * @return
     */
    public static Result success(Object data,String msg) {

        Result rb = new Result();
        rb.setCode(ResultInfo.SUCCESS.getCode());
        rb.setMessage(msg);
        rb.setData(data);
        return rb;

    }
    /**
     * 没有此数据、查询的数据为空
     * @param data
     * @return
     */
    public static Result nullData(Object data) {

        Result rb = new Result();
        rb.setCode(ResultInfo.NULLDATA.getCode());
        rb.setMessage(ResultInfo.NULLDATA.getMsg());
        rb.setData(data);
        return rb;

    }

    /**
     * 失败
     */
    public static Result error(ResultInfo errorInfo) {
        Result rb = new Result();
        rb.setCode(errorInfo.getCode());
        rb.setMessage(errorInfo.getMsg());
        rb.setData(null);
        return rb;
    }

    /**
     * 失败
     */
    public static Result error(int code, String message) {
        Result rb = new Result();
        rb.setCode(code);
        rb.setMessage(message);
        rb.setData(null);
        return rb;
    }

    /**
     * 失败
     */
    public static Result error(String message) {
        Result rb = new Result();
        rb.setCode(-1);
        rb.setMessage(message);
        rb.setData(null);
        return rb;
    }
}
