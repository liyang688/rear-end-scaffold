package com.zzu.common.dto;

import lombok.Data;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: ly
 * @Date: 2021/11/29/17:03
 * @Description:
 */
@Data
public class DeleteDto {

    //品牌
    private List<Integer> ids;
}
