package com.zzu.common.dto;

import com.alibaba.fastjson.JSONArray;
import lombok.Data;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: ly
 * @Date: 2021/11/29/17:03
 * @Description:
 */
@Data
public class TagsDto {

    //名字
    private String name;
    //类型
    private List<String> type;
    //成分
    private List<String> ingredient;
    //品牌
    private List<String> brand;
}
