package com.zzu.common.dto;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: ly
 * @Date: 2021/11/29/17:03
 * @Description:
 */
@Data
public class LoginDto {
    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

}
