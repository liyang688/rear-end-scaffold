package com.zzu.util;

import org.apache.http.entity.ContentType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: ly
 * @Date: 2021/12/13/19:38
 * @Description:
 */
public class FileUtil {
    public static MultipartFile fileToMultipartFile(String path) throws IOException {
        File f = new File(path);
        FileInputStream fileInputStream = new FileInputStream(f);
        MultipartFile multipartFile = new MockMultipartFile(f.getName(), f.getName(),
                ContentType.APPLICATION_OCTET_STREAM.toString(), fileInputStream);
        return multipartFile;
    }




}
