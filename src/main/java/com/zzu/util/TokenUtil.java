package com.zzu.util;



import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;
import com.zzu.entity.User;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;


public class TokenUtil {

    private static final long EXPIRE_TIME =7 * 60 * 24 * 60 * 1000;
    //private static final long EXPIRE_TIME =5 * 1000;
    private static final String TOKEN_SECRET = "token123";  //密钥盐

    /**
     * @return java.lang.String
     * @Author Core Miles
     * @createTime 2021/4/16 15:54
     * @params [user]
     */
    public static String sign(User user) {
        String token = null;
        try {
            Date expiresAt = new Date(System.currentTimeMillis() + EXPIRE_TIME);
            token = JWT.create()
                    .withIssuer("auth0").withKeyId(user.getUserId() + "")
                    .withClaim("username", user.getUsername())
                    .withExpiresAt(expiresAt)
                    // 使用了HMAC256加密算法。
                    .sign(Algorithm.HMAC256(TOKEN_SECRET));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return token;
    }

    /**
     * 获取当前时间戳(10位整数)
     */
    public static Long getCurrentTime() {
        return (System.currentTimeMillis() / 1000);
    }

    public static boolean isExpireToken(String token) {
        JWTVerifier verifier = JWT.require(Algorithm.HMAC256(TOKEN_SECRET)).withIssuer("auth0").build();
        DecodedJWT jwt = verifier.verify(token);

        Date expiresAt = jwt.getExpiresAt();
        Long currentTime = getCurrentTime();
        Date date = new Date(currentTime);
        //System.out.println("当前系统时间：" + date);
        //System.out.println("时间戳的时间：" + expiresAt);
        //如果参数 Date相等，则返回值 0；如果此 expiresAt 在 new Date(getCurrentTime() 参数之前，则返回小于 0 的值；若大于，则返回大于 0 的值。
        int i = expiresAt.compareTo(new Date(getCurrentTime()));// 对比当前时间
        //System.out.println("当前时间和时间戳时间的对比结果（>=0 为过期；<0 为有效）：" + i);
        if (i >= 0) {    // 如果过期 则函数返回true

            return true;
        }
        return false;
    }

    /**
     * @return boolean
     * @Author Core Miles
     * @createTime 2021/4/16 15:53
     * @params [token]
     */
    public static boolean verify(String token) {
        try {
            JWTVerifier verifier = JWT.require(Algorithm.HMAC256(TOKEN_SECRET)).withIssuer("auth0").build();
            DecodedJWT jwt = verifier.verify(token);
            //System.out.println("认证通过：");
            //System.out.println("issuer: " + jwt.getIssuer());
            //System.out.println("username: " + jwt.getClaim("username").asString());
            // 获取token中的id
            //System.out.println("id: " + jwt.getKeyId());
            //System.out.println("过期时间：      " + jwt.getExpiresAt());
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * @return java.lang.Integer
     * @Author Core Miles
     * @createTime 2021/4/16 15:52
     * @params [token]
     */
    public static Integer getCurrentUserId(HttpServletRequest httpServletRequest) {
        String token = httpServletRequest.getHeader("token");
        try {
            JWTVerifier verifier = JWT.require(Algorithm.HMAC256(TOKEN_SECRET)).withIssuer("auth0").build();
            DecodedJWT jwt = verifier.verify(token);
            //System.out.println("username: " + jwt.getClaim("username").asString());
            // 获取token中的id
            String keyId = jwt.getKeyId();
            Integer id = Integer.parseInt(keyId); //转为Integer类型
            //System.out.println("当前用户id: " + jwt.getKeyId());
            //System.out.println("过期时间：" + jwt.getExpiresAt());
            return id;
        } catch (Exception e) {
            return 0;
        }
    }

    /**
     * @return java.lang.String
     * @Author Core Miles
     * @createTime 2021/4/16 15:53
     * @params [token]
     */
    public static String getCurrentUsername(HttpServletRequest httpServletRequest) {
        String token = httpServletRequest.getHeader("token");
        //System.out.println("当前用户的token为：" + token);
        try {
            JWTVerifier verifier = JWT.require(Algorithm.HMAC256(TOKEN_SECRET)).withIssuer("auth0").build();
            DecodedJWT jwt = verifier.verify(token);
            String username = jwt.getClaim("username").asString();
            //System.out.println("当前用户名: " + username);
            //System.out.println("过期时间：      " + jwt.getExpiresAt());
            return username;
        } catch (Exception e) {
            return null;
        }
    }
}