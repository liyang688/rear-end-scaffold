package com.zzu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EqualDamageApplication {

    public static void main(String[] args) {
        SpringApplication.run(EqualDamageApplication.class, args);
    }

}
